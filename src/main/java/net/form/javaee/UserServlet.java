package net.form.javaee;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Servlet implementation class UserServlet
 */
@WebServlet("/netServlet")
public class UserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String firstName = request.getParameter("firstName");
		String lastName = request.getParameter("lastName");
		String email = request.getParameter("email");
		String phone = request.getParameter("phone");
		String city = request.getParameter("city");
		String country = request.getParameter("country");
		
		PrintWriter writer = response.getWriter();
		writer.println("<h1> Usuario Registrado </h1>");
		writer.println("<h3>Nombre: " + firstName + "</h3>");
		writer.println("<h3>Apellido: " + lastName + "</h3>");
		writer.println("<h3>Email: " + email + "</h3>");
		writer.println("<h3>Telefono: " + phone + "</h3>");
		writer.println("<h3>Ciudad: " + city + "</h3>");
		writer.println("<h3>Pais: " + country + "</h3>");
		writer.close();
	}

}
